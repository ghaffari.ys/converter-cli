#!/usr/bin/env node
'use strict';
import program from 'commander';
import chalk from'chalk';
import fs from 'fs';
import converter from 'converter';
import prompt from 'prompt';
import svg from 'pixelated-svg';
program.version('0.0.1');
// // program
// //     .option('-ts','--typesuport','xml,yaml,json')
// //     .option('-rt','--request type','request the type file that you need')
program.parse(process.argv);
    
if (!program.typesuport) {console.log('this type not suport')}
prompt.start();
prompt.get(['typeofentryfile','typeoffinalfile', 'pathfilestart','pathfileend'],function (err,result){
    console.log('entry type:' + result.typeofentryfile);
    console.log('require type:' + result.typeoffinalfile);
    //Convert between XML, JSON and YAML, from one format to another.
    var mojaz = ['xml','yaml','json'];
    // get a file stream reader pointing to the csv file to convert
    var reader = fs.createReadStream(result.pathfilestart);
    // get a file stream writer pointing to the json file to write to
    var writer = fs.createWriteStream(result.pathfileend);
    if (result.typeofentryfile in mojaz && result.typeoffinalfile in mojaz) {
    var format = {
        from : result.typeofentryfile ,
        to : result.typeoffinalfile
    };
    var convert = converter(format);
    }
});